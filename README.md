[![pipeline status](https://gitlab.com/louisa.natalika/story-8-ppw-ajax-django/badges/master/pipeline.svg)](https://gitlab.com/louisa.natalika/story-8-ppw-ajax-django/commits/master)
[![pipeline status](https://gitlab.com/louisa.natalika/story-8-ppw-ajax-django/badges/master/coverage.svg)](https://gitlab.com/louisa.natalika/story-8-ppw-ajax-django/commits/master)

# About
Book searcher Website using AJAX and Google Books API

## Heroku
[lika-books.herokuapp.com](lika-books.herokuapp.com)


