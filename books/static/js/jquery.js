let button = $('#button')
let table_content = $('tbody')
let search = $('#search-box')

function ajax_function(key){
    $('tbody').empty()
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
        success: function(response){
            console.log(response.items[0])
            for(let i = 0; i < response.items.length; i++){
                let tRow = document.createElement('tr')
                let tElement = document.createElement('td')
                tElement.innerHTML = i + 1
                
                let image = document.createElement('td')
                if (response.items[i].volumeInfo.imageLinks !== undefined){
                    let cover = document.createElement('img')
                    cover.src = response.items[i].volumeInfo.imageLinks.smallThumbnail
                    image.append(cover)
                } else{
                    let cover = document.createElement('p')
                    cover.innerHTML = 'Picture unavailable'
                    image.append(cover)
                }

                let titleTag = document.createElement('td')
                titleTag.innerHTML = response.items[i].volumeInfo.title

                let authorTag = document.createElement('td')
                if(response.items[i].volumeInfo.authors !== undefined){
                    for(let j = 0; j < response.items[i].volumeInfo.authors.length; j++){
                        let author = response.items[i].volumeInfo.authors[j]
                        authorTag.innerHTML = author
                    }
                } else{
                    authorTag.innerHTML = 'Author(s) unknown'
                }

                let descTag = document.createElement('td')
                if(response.items[i].volumeInfo.description !== undefined){
                    descTag.innerHTML = response.items[i].volumeInfo.description
                } else {
                    descTag.innerHTML = 'Description is unavailable'
                }

                tRow.append(tElement, image, titleTag, authorTag, descTag)
                table_content.append(tRow)
            }
        },

        error: function(){
            alert('Failed')
        }
    })
}

$(document).ready(ajax_function('leadership'))

button.click(function(){
    let keyword = search.val()
    ajax_function(keyword)
})