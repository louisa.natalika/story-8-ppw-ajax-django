from django.test import TestCase, Client
from django.urls import resolve
from .views import home

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time


# Create your tests here.
class UnitTestBooks(TestCase):
    def test_apakah_url_ada(self):
        c = Client()
        response = c.get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_url_menggunakan_fungsi_home(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_apakah_fungsi_home_bekerja_dengan_merender_html_home(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('about.html')

    def test_apakah_terdapat_search_box(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn('form', content)

    def test_apakah_terdapat_tombol_search(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn('button', content)
    
    def test_apakah_terdapat_table(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf-8')
        self.assertIn('table', content)


class Functional_Test(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get(self.live_server_url)
        self.assertIn("Book", self.browser.page_source)

    def test_terdapat_search_box_button_dan_table(self):
        self.browser.get(self.live_server_url)
        self.assertIn('form', self.browser.page_source)
        self.assertIn('button', self.browser.page_source)
        self.assertIn('table', self.browser.page_source)




